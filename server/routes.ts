import { Application } from 'express';
import ripley from './api/controllers/ripley/router';
export default function routes(app: Application): void {
  app.use('/api/v1/ripley', ripley);
}