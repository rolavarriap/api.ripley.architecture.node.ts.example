import RipleyService from '../../services/ripley.service';
import { Request, Response } from 'express';

export class Controller {
  all(req: Request, res: Response): void {
    RipleyService.all().then(r => {
      if (r) res.json(r);
      else res.status(500).end();
    });      
  }

  byId(req: Request, res: Response): void {
    const id = Number.parseInt(req.params['id'])
    RipleyService.byId(id).then(r => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }
  
  create(req: Request, res: Response): void {
    RipleyService.create(req.body).then(id =>
      res
        .status(201)
        .location(`/api/v1/ripley/${id}`)
        .send()
    );
  }
}
export default new Controller();
