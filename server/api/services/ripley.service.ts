import L from '../../common/logger';
import pool from '../../common/dbconfig';
import { Producto } from '../model/Producto';

const getAllQuery = {text: "select * from producto"}
const getProductById = {text: "select * from producto where id = $1"}
const insertProductoQuery = {text: "INSERT INTO producto(nombre,precio) values($1,$2) RETURNING ID"}

export class RipleyService {
  
  all(): Promise<Array<Producto>> {
    let prods = new Array<Producto>();
    return pool
    .query(getAllQuery)
    .then(res => {
      for(let p of res.rows){
        prods.push(p);
      }
      return Promise.resolve(prods);
    }).catch(e => {
      L.error(e.stack); 
      return Promise.reject(e.stack);
    });
  }

  byId(id: number): Promise<Producto> {
    L.info(`fetch example with id ${id}`);
    return pool.query(getProductById,[id])
    .then(res => {
      console.log(res.rows[0]);
      return Promise.resolve(res.rows[0] as Producto);
    }).catch(e => { 
      L.error(e.stack); 
      return Promise.reject(e.stack);
    });

  }

  create(prod: Producto): Promise<number> {
    L.info(`create producto with details ${prod}`);
    return pool.query(insertProductoQuery,[prod.nombre,prod.precio])
    .then(res => {
      return Promise.resolve(res.rows[0].id);
    }).catch(err =>{
      return Promise.reject(err.stack);
    });
    
  }
}

export default new RipleyService();
